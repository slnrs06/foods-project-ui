import { Injectable } from '@angular/core';
import { Country } from '../models/country';
import { Ingredients } from '../models/ingredients';
import { Food } from '../models/food';

@Injectable({
  providedIn: 'root'
})
export class DataTransferService {
  createCountry = false;
  createFood = false;
  createIngredient = false;

  country: Country;
  food: Food;
  ingredient:Ingredients;

  constructor() { }
}
