import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class SearchService {

  constructor(private http: HttpClient) { }
  searchIngredientsByCountryAndFood(counrtyNm: string, foodNm: string) {
    return this.http.get<any>(environment.url + 'ingredients/get-list-by/'+ counrtyNm + '/' + foodNm);
  }


}
