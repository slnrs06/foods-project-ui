import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Country } from '../models/country';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CountryService {

  constructor(private Http: HttpClient) { }
  getAllCountry(){
    return this.Http.get(environment.url + "country/all")as Observable<Country[]>;
}
postCountry(country: Country) {
    return this.Http.post(environment.url + "country/create", country) as Observable<Country>;
  }

  deleteCountry(id: number){
      return this.Http.delete(environment.url + "country/delete/" +
       id, {responseType: 'text'}) as Observable<any>;
  }

  updateCountry(id: number, country: Country){
      return this.Http.put(
      environment.url + "country/update/"+ id, country) as Observable<any>;
  }

 

}

 
//   constructor(private http: HttpClient) { }
//   createCountry(country: Country) {
//     return this.http.post(environment.url + 'country/create', country) as Observable<Country>;
//   }
//   updateCountry(country: Country) {
//     return this.http.put(environment.url + 'country/update', country) as Observable<Country>;
//   }
//   deleteCountry(id: number) {
//     return this.http.delete(environment.url + 'country/delete' + id) as Observable<Country>;
//   }
//   getAllCountry() {
//     return this.http.get(environment.url + 'country/all') as Observable<Country>;
//   }
// }

