import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Ingredients } from '../models/ingredients';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class IngredientsService {

  constructor(private Http: HttpClient) { }
  getAllIngredients(){
    return this.Http.get(environment.url + "ingredients/all")as Observable<Ingredients[]>;
}
postIngredients(ingredients: Ingredients) {
    return this.Http.post(environment.url + "ingredients/create", ingredients) as Observable<Ingredients>;
  }

  deleteIngredients(id: number){
      return this.Http.delete(environment.url + "ingredients/delete/" +
       id, {responseType: 'text'}) as Observable<any>;
  }

  updateIngredients(id: number, ingredients: Ingredients){
      return this.Http.put(
      environment.url + "ingredients/update/"+ id, ingredients) as Observable<any>;
  }

 

}

 





//   constructor(private http: HttpClient) { }
//   createIngredients(ingredients: Ingredients) {
//     return this.http.post(environment.url + 'ingredients/create/', ingredients) as Observable<Ingredients>;
//   }
//   createIngredientsList(list: Ingredients[]) {
//     return this.http.post(environment.url + 'ingredients/create-ingredients-list', list) as Observable<Ingredients[]>;
//   }
//   updateIngredients(ingredients: Ingredients) {
//     return this.http.put(environment.url + 'ingredients/update', ingredients) as Observable<Ingredients>;
//   }
//   deleteIngredients(id: number) {
//     return this.http.delete(environment.url + 'ingredients/delete/' + id) as Observable<Ingredients>;
//   }
//   getAllIngredients() {
//     return this.http.get(environment.url + 'ingredients/all') as Observable<Ingredients>;
//   }
// }
