import { Component, OnInit } from '@angular/core';
import { Food } from '../models/food';
import { FormGroup, FormBuilder } from '@angular/forms';
import { ApiService } from '../api.service';

@Component({
  selector: 'app-food',
  templateUrl: './food.component.html',
  styleUrls: ['./food.component.scss']
})
export class FoodComponent implements OnInit {
  result:Food[] = [];

  editFood = false;

  foodForm: FormGroup;
  constructor( 
    private fb: FormBuilder,
    private foodService: ApiService) { }

  ngOnInit(): void {
    this.getAll()
    this.foodForm = this.createFoodForm();
  }

  getAll(){
    this.foodService.getAll().toPromise().then((result)=>{
      this.result = result;
      console.log(result)
    })
  }

  createFoodForm(){
    return this.fb.group({
      id: [""],
      name: [""],
      countryid: [""],
      
    })
  }

  createPost() {
    const food =  new Food();
    
    food.name = this.foodForm.controls.name.value;
    food.countryid= this.foodForm.controls.countryid.value;
    this.foodService.post(food).toPromise()
    .then((res) => {
      this.result.push(res);
      console.log(res);
    });
  }


}
