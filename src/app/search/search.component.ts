import { Component, OnInit } from '@angular/core';
import { SearchService } from '../services/search.service';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit {

  constructor(private searchSvc: SearchService) { }

  ingredientsResults: any[] = [];
  foodName: string;
  country: string;

  ngOnInit(): void {
  }
  searchIngredientsBy(country, food) {
    this.searchSvc.searchIngredientsByCountryAndFood(country, food)
    .toPromise()
    .then((res) => {console.log(res)
      this.country = res;
      this.ingredientsResults = res;
    })
  }
}
