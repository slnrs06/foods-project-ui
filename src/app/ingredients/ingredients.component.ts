import { Component, OnInit } from '@angular/core';
import { Ingredients } from '../models/ingredients';
import { IngredientsService } from '../services/ingredients.service';
import { FormBuilder,FormGroup } from '@angular/forms';

@Component({
  selector: 'app-ingredients',
  templateUrl: './ingredients.component.html',
  styleUrls: ['./ingredients.component.scss']
})
export class IngredientsComponent implements OnInit {

  result : Ingredients[]=[];
  ingredientsForm: FormGroup;
  editIngredients = false;

  constructor(    private fb: FormBuilder,
    private ingredientsService: IngredientsService) { }

  ngOnInit(): void {
    this.getAll()
    this.ingredientsForm = this.createIngredientsForm();
  }

  getAll(){
    this.ingredientsService.getAllIngredients()
    .toPromise()
    .then((result) =>{
      this.result =result;
      console.log(result)
    })
  }


  createIngredientsForm(){
    return this.fb.group({
      id: [""],
      name: [""],
      foodid: [""],
    
      
    })
  }


  createPost() {
    const ingredients =  new Ingredients();
    
    ingredients.name = this.ingredientsForm.controls.name.value;
    ingredients.foodid = this.ingredientsForm.controls.foodid.value;
    this.ingredientsService.postIngredients(ingredients).toPromise()
    .then((res) => {
      this.result.push(res);
      console.log(res);
    });
  }


  edit(ingredients){
    this.editIngredients = true;
    this.ingredientsForm.setValue(ingredients);
  }
  saveUpdatedIngredients(){
    this.ingredientsService
    .updateIngredients(this.ingredientsForm.controls.id.value, this.ingredientsForm.value)
    .toPromise()
    .then((ingredients) => {
      this.getAll();
      this.ingredientsForm.reset();
      this.editIngredients=false;
    });
  }

  cancel(){
    this.ingredientsForm.reset();
    this.editIngredients=false;
  }

  delete(ingredients){
    this.ingredientsService
    .deleteIngredients(ingredients.id)
    .toPromise()
    .then((res) => {
      this.getAll()
      console.log("deleted")
    })
  }



}
