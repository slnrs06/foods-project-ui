import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Country } from '../models/country';
import { CountryService } from '../services/country.service';

@Component({
  selector: 'app-country',
  templateUrl: './country.component.html',
  styleUrls: ['./country.component.scss']
})
export class CountryComponent implements OnInit {
result : Country[]=[];
  countryForm: FormGroup;
  editCountry = false;

  constructor(
    private fb: FormBuilder,
    private countryService: CountryService){}
   
  ngOnInit(): void {
    this.getAll()
    this.countryForm = this.createCountryForm();
  }

  getAll(){
    this.countryService.getAllCountry()
    .toPromise()
    .then((result) =>{
      this.result =result;
      console.log(result)
    })
  }


  createCountryForm(){
    return this.fb.group({
      id: [""],
      name: [""],
    
      
    })
  }


  createPost() {
    const country =  new Country();
    
    country.name = this.countryForm.controls.name.value;
    
    this.countryService.postCountry(country).toPromise()
    .then((res) => {
      this.result.push(res);
      console.log(res);
    });
  }


  edit(country){
    this.editCountry = true;
    this.countryForm.setValue(country);
  }
  saveUpdatedCountry(){
    this.countryService
    .updateCountry(this.countryForm.controls.id.value, this.countryForm.value)
    .toPromise()
    .then((country) => {
      this.getAll();
      this.countryForm.reset();
      this.editCountry=false;
    });
  }

  cancel(){
    this.countryForm.reset();
    this.editCountry=false;
  }

  delete(country){
    this.countryService
    .deleteCountry(country.id)
    .toPromise()
    .then((res) => {
      this.getAll()
      console.log("deleted")
    })
  }



}
