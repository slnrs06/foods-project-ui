import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { Food } from './models/food';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(private Http: HttpClient) { }
  getAll(){
    return this.Http.get(environment.url + "foods/all")as Observable<Food[]>;
}
post(food: Food) {
    return this.Http.post(environment.url + "foods/create", food) as Observable<Food>;
  }

  delete(id: number){
      return this.Http.delete(environment.url + "foods/delete/" +
       id, {responseType: 'text'}) as Observable<any>;
  }

  update(id: number, food: Food){
      return this.Http.put(
      environment.url + "foods/update/"+ id, food) as Observable<any>;
  }

}


