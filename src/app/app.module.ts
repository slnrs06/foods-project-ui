import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';
import { FoodComponent } from './food/food.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import {MatButtonModule} from '@angular/material/button';
import { IngredientsComponent } from './ingredients/ingredients.component';
import { CountryComponent } from './country/country.component';
import { SearchComponent } from './search/search.component';
import { TasteTheWorldComponent } from './taste-the-world/taste-the-world.component';
import { ModalsDialogComponent } from './modals/modals-dialog/modals-dialog.component';


@NgModule({
  declarations: [
    AppComponent,
    FoodComponent,
    IngredientsComponent,
    CountryComponent,
    SearchComponent,
    TasteTheWorldComponent,
    ModalsDialogComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    BrowserAnimationsModule,
    NgbModule,
    

  ],
  providers: [],

  bootstrap: [AppComponent]
})
export class AppModule { }
